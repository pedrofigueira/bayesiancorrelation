"""
Written by Pedro Figueira. 
Original version can be found at https://pedrofigueira@bitbucket.org/pedrofigueira/bayesiancorrelation
"""

import string, sys
import numpy as np

from scipy.stats import rankdata, chi2, norm
from scipy.stats.stats import pearsonr, spearmanr

from pylab import hist, show

import pymc as pm
from pymc.Matplot import plot as mcplot
    
def MCMC_model(inputfile=[], spearmanrank=False, save_posterior=False, analytical=True):
    """
    MCMC calculation for the BiVariate Normal distribution assumed
        parameters:
        inputfile: x,y tab-separated file with the data
                    -- or --
                   [] for test running (default)
        spearmanrank: flag to toggle pearsonr calculation (False, set by 
                        default) or spearman's rank (True)
        save_posterior: flag to save the posterior distribution ASCII one-column file.
        analytical: flag to perform an analytical calculation of the posterior 
    """
        
    if(inputfile==[]):
        print "Reading test data."
        #example from http://sumsar.net/blog/2014/03/bayesian-first-aid-pearson-correlation-test/
        x_input, y_input = np.loadtxt("testdata_male.txt", unpack=True)
    else:
        x_input, y_input = np.loadtxt(inputfile, unpack=True)
        
    x_input = np.array(x_input)
    y_input = np.array(y_input)

    print("The pearson's coefficient of the data is %.3f with a %.3f 2-sided p-value " % (pearsonr(x_input, y_input)[0], pearsonr(x_input, y_input)[1]))
    print("The spearman's rank coefficient of the data is %.3f with a %.3f 2-sided p-value " % (spearmanr(x_input, y_input)[0], spearmanr(x_input, y_input)[1]))
    
    if(spearmanrank):
        #rank variable for spearman's rank calculation
        x_input = rankdata(x_input)
        y_input = rankdata(y_input)
        
    #creating standartized data sets (that does not affect pearson's rho)
    x = (x_input - x_input.mean()) / x_input.std()     
    y = (y_input - y_input.mean()) / y_input.std()
    
    #beginning of data analysis
    data = np.array([x, y])
    
    #For a description of the distributions available check: 
    # https://pymc-devs.github.io/pymc/distributions.html    
    
    # assume an uniformative prior for rho between -1 and 1 and starting at 0 for the sampling
    rho = pm.Uniform('rho', lower=-1, upper=1, value=0.0)  
    # the priors on our expected data values, and note the starting point derived from the data
    mu1 = pm.Normal('mu1', 0.0, 1.0/(1000)**2.0, value = x.mean())        
    mu2 = pm.Normal('mu2', 0.0, 1.0/(1000)**2.0, value = y.mean())
    sigma1 = pm.InverseGamma('sigma1', 11.0, 10.0, value = x.std())
    sigma2 = pm.InverseGamma('sigma2', 11.0, 10.0, value = y.std())

    # deterministic functions to use with BiVariate Normal
    @pm.deterministic
    def mean(mu1=mu1, mu2=mu2):
        return np.array([mu1, mu2])    
        
    @pm.deterministic
    def CoV(rho=rho, sigma1=sigma1, sigma2=sigma2):
        ss1 = sigma1 * sigma1
        ss2 = sigma2 * sigma2
        rss = rho * sigma1 * sigma2
        return [[ss1, rss], [rss, ss2]]

    #BiVariate normal definition
    xy = pm.MvNormalCov('xy', mu=mean, C=CoV, value=data.T, observed=True)
    
    #use the previously declared local variables as input to the model
    model = pm.Model([xy, rho, mu1, mu2, sigma1, sigma2])
    #call mapping routine to find maximum a posteriori
    pm.MAP(model).fit()
      
    # start the mcmc and sample the results  
    mcmc = pm.MCMC(model)
    mcmc.sample(iter = 100000, burn = 50000, thin = 10)

    if(analytical):
        print ("\n"*2) + ("-" * 66) 
        print "Performing analytical calculation..." 
        rho_post = calculate_posterior(pearsonr(x_input, y_input)[0], len(x_input))
        print("The mean, std and 95%% HPD are %.3f, %.3f, and [%.3f, %.3f]." % (np.average(rho_post), np.std(rho_post), pm.utils.hpd(rho_post[:], 1.0-0.95)[0], pm.utils.hpd(rho_post[:], 1.0-0.95)[1]))
        print ("-" * 66) + ("\n"*2) 
    return mcmc    

def calculate_posterior(r, n, a=1, b=2, N=10000):
    # function for Analytical posterior calculation

    Z = norm.rvs(size=N)
    chi2_1 = chi2.rvs(n-a, size=N)
    chi2_2 = chi2.rvs(n-b, size=N)

    Y = - Z/np.sqrt(chi2_1) + (np.sqrt(chi2_2) / np.sqrt(chi2_1)) * (r / np.sqrt(1-r**2))
    rho_post = Y / np.sqrt(1+Y**2.0)
    return rho_post

###############################################################################                        
                        
if __name__ == "__main__":
    if(len(sys.argv) == 1):    
        mcmc = MCMC_model()          #applying the mcmc to our model and recording values
    elif(len(sys.argv) == 2):       #then we have a filename but no option string
        mcmc = MCMC_model(inputfile = sys.argv[1]) 
        mcmc.write_csv(string.split(sys.argv[1], ".")[0]+"_rho_summary.csv", variables=["rho"]) #saving the results on .csv format
    elif(len(sys.argv) == 3):       # identification of an option string
        if(sys.argv[2].lower() in ["r", "rs"]):    #analyze ranked data
            mcmc = MCMC_model(inputfile = sys.argv[1], spearmanrank = True)
        else:
            mcmc = MCMC_model(inputfile = sys.argv[1])
        mcmc.write_csv(string.split(sys.argv[1], ".")[0]+"_rho_summary.csv", variables=["rho"]) #saving the results on .csv format
    mcmc.summary()      #displaying the results for rho on screen    
    mcplot(mcmc, format='pdf')            #plotting and saving of figures for visually checking the convergence
    if(len(sys.argv) == 3):
        if(sys.argv[2].lower() in ["s", "rs"]): #save the posterior distribution
            np.savetxt(string.split(sys.argv[1], ".")[0]+"_postdist.txt", mcmc.trace('rho')[:])
    show()